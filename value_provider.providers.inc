<?php
/**
 * @file
 * Value Providers - Semantic result values for programmatic Views usage.
 *
 * Copyright 2008 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of hook_value_provider_info():
 * Return a list of default value providers.
 */
function value_provider_value_provider_info() {
  $providers = array();
  $supported_field_types = array(
    'number_float', 'number_integer', 'number_decimal',
    'nodereference', 'userreference',
  );

  if (module_exists('content')) {
    foreach (content_fields() as $field) {
      if (!in_array($field['type'], $supported_field_types)) {
        continue;
      }
      $provider = value_provider_content_provider_base($field);

      switch ($field['type']) {
        case 'nodereference':
          $provider['parse callback'] = 'value_provider_nodereference_parse_nid';
          $providers['nid'][$field['field_name']] = $provider;
          break;

        case 'userreference':
          $provider['parse callback'] = 'value_provider_userreference_parse_uid';
          $provider['filters']['uid'] = array(
            'field' => $field['field_name'] . '_uid',
            'callback' => 'value_provider_userreference_filter_uid',
          );
          $provider['filters']['has_uid'] = array(
            'field' => $field['field_name'] . '_uid',
            'callback' => 'value_provider_userreference_filter_has_uid',
          );
          $providers['uid'][$field['field_name']] = $provider;
          break;

        case 'number_float':
        case 'number_integer':
        case 'number_decimal':
          $provider['parse callback'] = 'value_provider_number_parse_value';
          $providers['number'][$field['field_name']] = $provider;
          break;
      }
    }
  }
  return $providers;
}

function value_provider_number_parse_value($item) {
  return empty($item['value']) ? 0 : $item['value'];
}

function value_provider_nodereference_parse_nid($item) {
  return empty($item['nid']) ? 0 : $item['nid'];
}

function value_provider_userreference_parse_uid($item) {
  return empty($item['uid']) ? 0 : $item['uid'];
}

function value_provider_userreference_filter_uid(&$filter, $filter_handler, $filter_options) {
  $filter['operator'] = 'or';
  $filter['value'] = array($filter_options); // where $filter_options == $uid
}

function value_provider_userreference_filter_has_uid(&$filter, $filter_handler, $filter_options) {
  $filter['operator'] = 'or';

  if ($filter_options == TRUE) { // has uid
    $filter_handler->get_value_options();
    $filter['value'] = array_keys($filter_handler->value_options);
  }
  else { // has no uid
    $filter['value'] = array();
  }
}
